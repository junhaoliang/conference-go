from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_location_pic(query):
    headers = {"Authorization": PEXELS_API_KEY}
    url = f"https://api.pexels.com/v1/search?query={query}"
    response = requests.get(url, headers=headers)
    api_dict = response.json()

    return api_dict["photos"][0]["src"]["original"]


def get_weather_data(city, state):
    country_code = "US"
    coor_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},{country_code}&appid={OPEN_WEATHER_API_KEY}"
    coor_response = requests.get(coor_url)
    api_lst = coor_response.json()
    if api_lst:
        lat = api_lst[0]["lat"]
        lon = api_lst[0]["lon"]
    else:
        return None

    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    weather_response = requests.get(weather_url)
    api_dt = weather_response.json()

    return {
        "temp": api_dt["main"]["temp"],
        "description": api_dt["weather"][0]["description"],
    }
